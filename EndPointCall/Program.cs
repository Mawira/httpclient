﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace EndPointCall
{
    class Program
    {
        static void Main(string[] args)
        {

            string date = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
            //  DateTime date=DateTime.UtcNow;
            Console.WriteLine(date);
            // Get();
            //  Post();
            Console.ReadLine();
        }

        public static async void Get()
        {
            // string path = "http://localhost:53030/home/AddUser";
            // string path = "https://sokokapu.com/api/countries/all";
            string path = "https://localhost:5001/Api/customers/CurrentCustomers";
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(path))
            using (HttpContent content = response.Content)
            {
                string data = await content.ReadAsStringAsync();
                Console.WriteLine(data);
            }

        }

        public static async void Post()
        {

            var user = new Seller()
            {
                FirstName = "sdsdssd",
                LastName = "sdwdww",
                MobilePhone = "0702382596",
                OfficePhone = "070000000"
            };



            ;
            HttpClient client = new HttpClient();
            var data = JsonConvert.SerializeObject(user);

            HttpContent content = new StringContent(data);
            client.BaseAddress = new Uri("http://localhost:9001/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            content.Headers.ContentType.MediaType = "text/json";
            HttpResponseMessage response = await client.PostAsync("api/sellers/add", content);
            Console.WriteLine(response.StatusCode);

            //======> solution for
            //System.InvalidOperationException: This instance has already
            //started one or more requests. Properties can only be modified before sending the first request

            //HttpClient.DefaultRequestHeaders(and BaseAddress) should only be set once, before you make any requests.HttpClient is only safe to use as a singleton if you don't modify it once it's in use.
            // Rather than setting DefaultRequestHeaders, set the headers on each HttpRequestMessage you are sending

            //var request = new HttpRequestMessage(HttpMethod.Post, url);
            //request.Headers.Accept.Clear();
            //request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            //request.Content = new StringContent("{...}", Encoding.UTF8, "application/json");
            //var response = await _client.SendAsync(request, CancellationToken.None);

            // Replace "{...}" with your JSON.

        }

    }
    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }

    public class Seller
    {
        public double Rating { get; set; }
        public string BusinessName { get; set; }

        public int NumberOfWishlist { get; set; }
        public string Address { get; set; }
        public string Box { get; set; }
        public string OfficePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Paybill { get; set; }
        public string Email { get; set; }

        public string Name { get; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Zip { get; set; }
        public int NumberOfOrders { get; set; }
    }
}
